import logging
from datetime import datetime, timedelta
from typing import Optional

import jwt
from fastapi import HTTPException, Depends, Security
from fastapi.security import OAuth2PasswordBearer, SecurityScopes
from jwt import PyJWTError
from passlib.context import CryptContext
from pydantic import ValidationError
from starlette.status import HTTP_401_UNAUTHORIZED, HTTP_400_BAD_REQUEST

from model import User, UserInDb, TokenData
from users import user_base, READ_SELF_INFO, READ_ITEM_LIST

available_roles = {READ_SELF_INFO: 'Read info about current user',
                   READ_ITEM_LIST: 'Read item list'}

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth",
                                     scopes=available_roles)


class Authorization:
    def __init__(self):
        self.pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
        self.SECRET_KEY = '59888d35b0791dc66dcf8cfaf4fc3242474b18821a1a1906f702faa0ce1d98eb'
        self.ALGORITHM = 'HS256'
        self.ACCESS_TOKEN_EXPIRE_MINUTES = 2
        self.loggger = logging.getLogger(__name__)

    def create_access_token(self, *, data: dict, expires_delta: timedelta = None):
        to_encode = data.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=self.ACCESS_TOKEN_EXPIRE_MINUTES)
        to_encode.update({'exp': expire})
        encoded_jwt = jwt.encode(to_encode, self.SECRET_KEY, algorithm=self.ALGORITHM)
        self.loggger.debug(f'created token: {to_encode}')
        return encoded_jwt

    def authenticate(self, username: str, password: str) -> Optional[User]:
        user = self.get_user(user_base, username)
        if not user:
            return None
        if not self.verify_password(password, user.hashed_password):
            return None
        return user

    def verify_password(self, plain, hashed):
        return self.pwd_context.verify(plain, hashed)

    def get_password_hash(self, password):
        return self.pwd_context.hash(password)

    def get_current_user(self, security_scopes: SecurityScopes, token: str = Depends(oauth2_scheme)):
        if security_scopes.scopes:
            authenticate_value = f'Bearer scope="{security_scopes.scope_str}"'
        else:
            authenticate_value = 'Bearer'
        credentials_exception = HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail='cannot validate credentials',
            headers={'WWW-Authenticate': authenticate_value}
        )
        try:
            payload = jwt.decode(token, self.SECRET_KEY, algorithms=[self.ALGORITHM])
            self.loggger.debug(f'user: {payload.get("sub")} roles: {payload.get("scopes")} expiry date: {datetime.utcfromtimestamp(payload.get("exp"))}')
            username: str = payload.get('sub')
            if username is None:
                raise credentials_exception
            token_scopes = payload.get('scopes', [])
            token_data = TokenData(scopes=token_scopes, username=username)
        except (PyJWTError, ValidationError) as e:
            raise credentials_exception
        user = self.get_user(user_base, username=username)
        if user is None:
            raise credentials_exception
        for scope in security_scopes.scopes:
            if scope not in token_data.scopes:
                self.loggger.warning(f'user {user} has no permissions for {scope}')
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED,
                                    detail='Insufficient permissions',
                                    headers={'WWW-Authenticate': authenticate_value})
        return user

    @staticmethod
    async def get_current_user_active(current_user: User = Depends(get_current_user)):
        if current_user.disabled:
            raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail='User inactive')
        return current_user

    @staticmethod
    def get_user(db, username: str) -> UserInDb:
        if username in db:
            dict_user = db[username]
            return UserInDb(**dict_user)
