READ_SELF_INFO = 'about_me'
READ_ITEM_LIST = 'items'

user_base = {
    "test": {
        "username": "test",
        "email": "test@testing.ground.com",
        "full_name": "Testing Tester",
        "hashed_password": '$2b$12$Eq5cMaFJfa1sPEQxgnMA5ecFJKKj.XXASJMS/ywmfxwoEwXukRd3m',
        "roles": [READ_SELF_INFO, READ_ITEM_LIST],
        "disabled": False
    },
    "johndoe": {
        "username": "johndoe",
        "email": "john@does.com",
        "full_name": "John Doe",
        "hashed_password": '$2b$12$Eq5cMaFJfa1sPEQxgnMA5ecFJKKj.XXASJMS/ywmfxwoEwXukRd3m',
        "roles": [],
        "disabled": False
    },
    "alice": {
        "username": "alice",
        "email": "alice@wonderland.com",
        "full_name": "Alice Wonderson",
        "hashed_password": '$2b$12$Eq5cMaFJfa1sPEQxgnMA5ecFJKKj.XXASJMS/ywmfxwoEwXukRd3m',
        "roles": [READ_SELF_INFO],
        "disabled": False
    }
}
