#!/usr/bin/env bash

if [ "$1" == "--build" ]
then
    docker build -t oauth .
fi
docker run -it --name dev --mount type=bind,source="$(pwd)",target=/app --rm -p 8000:8000 oauth
