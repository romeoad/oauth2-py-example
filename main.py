#!/usr/bin/env python
from datetime import timedelta

import uvicorn
from fastapi import Depends, FastAPI, HTTPException, Security
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_401_UNAUTHORIZED

from auth_service import Authorization
from log_config import LOGGING_CONFIG
from model import User, Token
from users import READ_SELF_INFO, READ_ITEM_LIST

app = FastAPI(title="OAuth2 protected example", version="1.0.0")
auth = Authorization()


@app.post("/auth", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user = auth.authenticate(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=auth.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = auth.create_access_token(
        data={"sub": user.username, 'scopes': user.roles}, expires_delta=access_token_expires
    )
    return Token(access_token=access_token, token_type="Bearer")


@app.get("/users/me", response_model=User)
async def read_users_me(current_user: User = Security(auth.get_current_user, scopes=[READ_SELF_INFO])):
    return current_user


@app.get('/items')
async def read_items(current_user: User = Security(auth.get_current_user, scopes=[READ_ITEM_LIST])):
    return [
        {"id": "Item1", "owner": current_user.username},
        {"id": "Item2", "owner": current_user.username}
    ]


if __name__ == '__main__':
    uvicorn.run('main:app', host='0.0.0.0', port=8000, reload=True, log_config=LOGGING_CONFIG)
