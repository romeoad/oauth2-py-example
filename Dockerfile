FROM python:3.8
COPY requirements.txt /
RUN pip install -r /requirements.txt
EXPOSE 8000
CMD ["/app/main.py"]
